## String Compression
Since the length of the string is variable, I will explain the time complexity using examples with different values for the
string. 
  There are two nested loops to take into consideration:
```python
while j < len(string) - 1:              # Loop over the length of the current list (Loop1)
    while string[i] == string[j]:       # Count as long as the next characters match the current one (Loop2)
```

### 'aaaa'
Loop2 will loop over the entire string because it is all the same character.
  Loop1 will only last for one iteration because the array will have been compressed beyond j.
  So this looped for n iterations (O(n)).

### 'abcde'
Loop1 will loop over the entire string because n will not change.
  Loop2 will only last for one iteration because each character is different.
  So this looped for n iterations (O(n)).

### 'aaabbccc'
Loop1's iterations will be equal to the length of the compressed string which is equal to the number of new characters encountered (< n).
  Loop2's iterations will be equal to the number of repetitions of each character (< n)
  So this looped for n + m iterations (O(n)).

__Therefore,__ `compress` time complexity is O(n).


## Network Failure Point
Let the number of key = n and the number of values = m.
```python
for router in network:                              # Loop over the keys in the original graph
```
O(n)


```python
for conn in conns:                              # Loop over the values of the current key
```
O(m)

```python
max_conns = max(vals)                               # Max number of connections
```
O(m)

```python
occ = vals.count(max_conns)                         # Occurrences of the max value
```
O(m)

```python
for router, conns in connections.items():
```
O(n)

__Therefore,__ `identify_router` time complexity is O(n*m).








