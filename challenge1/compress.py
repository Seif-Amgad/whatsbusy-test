s1 = 'bbcceeee'
s2 = 'aaabbbcccaaa'
s3 = 'a'


def compress(string):
    string = list(string)                   # Convert the string to a list to be able to change and delete characters   
    i = 1                                   # Counter starts from the current char + 1
    j = 0                                   # Current char index
    while j < len(string) - 1:              # Loop over the length of the current list
        while string[i] == string[j]:       # Count as long as the next characters match the current one
            i += 1
            if i >= len(string):            # Break when the counter is out of bounds
                break

        if i > j + 1:                       # If the counter is not the same
            string[j + 1] = str(i - j)      # Set the next char to the counter minus the index of the current char
            del string[j + 2: i]            # Remove repeated chars
            j += 2                          # Start next iteration from the char after the number

        else:                               # The counter hasn't counted
            j += 1                          # Start next iteration from the next char

        i = j + 1

    return "".join(string)                  # Convert the list to a string and return it


print(compress(s1))
print(compress(s2))
print(compress(s3))

