graph = {'1': ['2', '3'],
         '2': ['3', '4'],
         '3': ['4'],
         '4': ['3', '5', '6'],
         '5': ['6'],
         '6': ['3']
         }


def identify_router(network):
    max_routers = list()
    connections = dict.fromkeys(network, 0)             # A dict with the numbers of connections of each router
    for router in network:                              # Loop over the keys in the original graph
        conns = network[router]                         # Store values of current key
        connections[router] += len(conns)               # Add the inbound links to the corresponding key

        for conn in conns:                              # Loop over the values of the current key
            connections[conn] += 1                      # Count the outbound link for each key occurring

    vals = list(connections.values())
    max_conns = max(vals)                               # Max number of connections
    occ = vals.count(max(vals))                         # Occurrences of the max value
    seen = 0                                            # Counter for the number of times the max value was found
    for router, conns in connections.items():
        if conns == max_conns:
            max_routers.append(router)
            seen += 1
            if seen == occ:
                break

    return max_routers


print(identify_router(graph))