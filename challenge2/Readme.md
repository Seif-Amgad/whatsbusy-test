## How to use
1. Clone the repository.

2. Install the requirements.

        pip install requirements.txt
    
3. Sign up for [Stripe](https://stripe.com/) as a developer and get your API keys from the Dashboard.

4. Put the secret key at the bottom of `mysite/settings.py`.

        import stripe

        stripe.api_key = '{ Your sk }'
        
5. Put the publishable key in `edit_card.js` and `credit_card_info.js` in the `account/static/account/` directory.

        $(document).ready(function() {
            var stripe = Stripe('{ Your pk}');   // Stripe's publishable key

            var elements = stripe.elements();    // Stripe's elements

6. Run `create_product.py' to create a product and a price in __Stripe__.

        import stripe

        stripe.api_key = '{ Your sk }'

        product = stripe.Product.create(
          name='WhatsBusy Membership',
        )

        price = stripe.Price.create(
          product=product.id,
          unit_amount=4999,
          currency='usd',
          recurring={
            'interval': 'month',
          },
        )

        print(price.id)

7. Get the price id printed and set it at the bottom of `mysite/settings.py` like so:

        MEMBERSHIP_PRICE_ID = '{ Your price id }'

8. You need to have a Postgres database because the subscription model uses an __ArrayField__. So set the database credentials in `mysite/settings.py`.

        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql',
                'NAME': '{ db name }',
                'USER': '{ db user }',
                'PASSWORD': '{ db password }',
                'HOST': '{ db host }',
                'PORT': '{ db port }',
                'CONN_MAX_AGE': 500
            }
        }

9. Navigate to the root project directory in the terminal and migrate.

        python manage.py migrate
        
10. Test the website using __Stripe__'s [testing](https://stripe.com/docs/testing) cards.